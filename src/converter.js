/**
 * Padding outputs 2 characters always
 * @param {hex} Int one or two characters
 * @returns {hex} hex with two characters always
 */

module.exports = {
    hexToRbg: (red, green, blue) => {
        
        const redRgb = red.toInt(16); //0-255 -> 0-ff
        const greenRgb = green.toInt(16); // 0-255 -> 0-ff
        const blueRgb = blue.toInt(16); // 0-255 -> 0-ff
        return pad(redRgb) + pad(greenRgb) + pad(blueRgb); 
    }
}